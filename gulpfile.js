/*========================================
 =            Requiring stuffs            =
 ========================================*/

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    del = require('del'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify');

/*================================================
 =            Report Errors to Console            =
 ================================================*/

gulp.on('error', function (e) {
    throw(e);
});

/*=========================================
 =            Clean dest folder            =
 =========================================*/

gulp.task('clean', function (cb) {
    del(['dist/**'], cb);
});

/*=========================================
 =            Build dist files             =
 =========================================*/

gulp.task('js', function () {
    return gulp.src('src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(concat('dw-ng-commons.js'))
        .pipe(gulp.dest('dist'));
});

/*====================================
 =            Default Task            =
 ====================================*/

gulp.task('default', ['js']);