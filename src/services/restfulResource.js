'use strict';

angular.module('dw-ng-commons', ['ngResource'])
    .factory('restfulResource', function ($resource)
    {
        return function (url, params, actions)
        {
            var defaultActions = {
                update: {method: 'PUT', isArray: false},
                create: {method: 'POST'}
            };

            actions = angular.extend(defaultActions, actions);

            var resource = $resource(url, params, actions);

//    resource.prototype.$save = function(params, success, error) {
//      console.log('aaa')
//      return this.id ? this.$update(params, success, error) : this.$create(params, success, error);
//    };

            return resource;
        };

    });